export default bots => (req, res, next) => {
    // TODO: should this be a merge to be usable in multiple places?
    req.bots = bots;

    next();
};
