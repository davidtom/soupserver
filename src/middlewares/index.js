import * as bodyParser from 'body-parser';

import { reportingRequestMiddleware } from '../util/errorReporting';
import bots from '../bots';

import addBots from './addBots';

export default [
    reportingRequestMiddleware(),
    bodyParser.json(),
    addBots(bots)
];
