// eslint-disable-next-line import/prefer-default-export
export const WEEKLY_WOW_THREAD_TRIGGERS = {
    pvp: 'flair:PVP+Sunday',
    newb: 'flair:Murloc+Monday',
    tank: 'flair:Tanking+Tuesday',
    heals: 'flair:Midweek+Mending',
    loots: 'flair:Loot+Thread',
    dps: 'flair:Firepower+Friday'
};
