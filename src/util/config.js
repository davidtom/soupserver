// NOTE: config should NOT import any other modules to avoid missing env vars
import * as dotenv from 'dotenv';

const result = dotenv.config(); // default config loads .env from process.cwd()

if (process.env.NODE_ENV === 'development' && result.error) {
    throw result.error;
}

const config = result.parsed;

export default () => config;
