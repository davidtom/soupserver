const DEPLOYED_ENVIRONMENTS = [
    'production'
];

export function inDeploymentEnvironment(env) {
    return DEPLOYED_ENVIRONMENTS.indexOf(env) !== -1;
}

export function getRandomElement(list) {
    return list[Math.floor(Math.random() * list.length)];
}

export const msTimeConversion = (millisec) => {
    if (Number.isNaN(millisec)) {
        return '[ unknown ]';
    }

    const seconds = (millisec / 1000).toFixed(1);

    const minutes = (millisec / (1000 * 60)).toFixed(1);

    const hours = (millisec / (1000 * 60 * 60)).toFixed(1);

    const days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

    if (seconds < 60) {
        return `${seconds} Sec`;
    } else if (minutes < 60) {
        return `${minutes} Min`;
    } else if (hours < 24) {
        return `${hours} Hrs`;
    }

    return `${days} Days`;
};
