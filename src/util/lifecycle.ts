import * as terminus from "@godaddy/terminus";

async function onSignal(getCleanupPromise: () => Promise<void[]>): Promise<void> {
    console.log('[lifecycle] server is starting clean up');
    await getCleanupPromise();
}

async function onShutdown(): Promise<void> {
    console.log('[lifecycle] clean up finished, server is shutting down');
}

async function livenessCheck(): Promise<void> {
    return undefined;
}

async function readinessCheck(): Promise<void> {
    return undefined;
}

/**
 * Return a config object for @godaddy/terminus
 *
 * @param `getCleanupPromise` - a callback function that returns a promise; the server will
 *  wait for this promise to resolve before finishing cleanup, if/when possible
 */
export const getLifecycleConfig = (getCleanupPromise: () => Promise<void[]>): terminus.TerminusOptions  => ({
    healthChecks: {
        '/liveness_check': livenessCheck,
        '/readiness_check': readinessCheck
    },

    timeout: 25000,
    signals: [ 'SIGTERM' ],
    onSignal: () => onSignal(getCleanupPromise),
    onShutdown,

    logger: console.error
});
