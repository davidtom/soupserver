import { filter, tap } from 'rxjs/operators';
// import logger from 'util/logger';

export default triggerString => source => source.pipe(
    filter(({ message }) => message.content.startsWith(triggerString)),
    tap(({ bot }) => {
        // TODO: LOGGER!
        console.log(`responded to trigger: ${triggerString}`);
        bot.incrementMessageCounter(triggerString);
    })
);
