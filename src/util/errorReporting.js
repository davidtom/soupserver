import * as Sentry from '@sentry/node';

import { inDeploymentEnvironment } from './helpers';

const {
    NODE_ENV,
    SENTRY_DSN,
    RELEASE_VERSION
} = process.env;

if (inDeploymentEnvironment(NODE_ENV)) {
    const config = {
        dsn: SENTRY_DSN,
        environment: NODE_ENV,
        release: RELEASE_VERSION
    };

    Sentry.init(config);
}

export const reportingRequestMiddleware = Sentry.Handlers.requestHandler;
export const reportingErrorMiddleware = Sentry.Handlers.errorHandler;

export default (...args) => Sentry.captureException(...args);
