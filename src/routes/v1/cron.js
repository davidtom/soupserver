import cronController from '../../controllers/cron';

export default (app) => {
    app.get('/v1/cron', cronController.get);
    app.get('/v1/cron/weeklywow', cronController.getWeeklyWow);
};
