import cronRoutes from './cron';

export default (app) => {
    cronRoutes(app);
};
