import v1Route from './v1';

export default (app) => {
    v1Route(app);
};
