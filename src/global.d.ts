declare namespace SoupBot {
    interface HttpError extends Error {
        status?: number
    }
}
