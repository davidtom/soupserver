import * as http from 'http';
import * as express from 'express';
import { createTerminus } from '@godaddy/terminus';

import './util/config';

import middlewares from './middlewares';
import routes from './routes';
import bots from './bots';
import { getLifecycleConfig } from './util/lifecycle';
import { reportingErrorMiddleware } from './util/errorReporting';

// basic set up
const app: express.Express = express();

const env: string = process.env.NODE_ENV;
const port: string = process.env.PORT || String(8080);

app.use(middlewares);
routes(app);

// invalid route handler (404) and error handler (500)
app.use((req: express.Request, res: express.Response) => {
    console.log(`[server] path: ${req.originalUrl} not found`);

    res.status(404).json({
        error: 'requested URL not found'
    });
});

app.use(reportingErrorMiddleware());

// error handling
app.use((err: SoupBot.HttpError, req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.error('[server] default error handler caught error', {
        status: err.status,
        message: err.message,
        stack: err.stack
    });

    if (res.headersSent) {
        return next(err);
    }

    return res.status(500).json('internal server error');
});

// start server
const server: http.Server = http.createServer(app);

// set up shutdown procedure
const getCleanupPromise: () => Promise<void[]> = () => {
    // TODO: type bots
    // TODO: debug why i get an eventemitter/memory leak error
    const closePromises: Promise<void>[] = Object.keys(bots)
        .map(bot => bots[bot].close());

    // return bots.soupBot.close();
    return Promise.all([
        ...closePromises
    ]);
};

createTerminus(server, getLifecycleConfig(getCleanupPromise));

server.listen(port, () => {
    console.log(`[server] running in ${env} environment`);
    console.log(`[server] listening on port ${port}`);
});

// TODO: debug this - is # of event emitters going to be an issue?
process.on('warning', e => console.warn(e.stack));
