import { WEEKLY_WOW_THREAD_TRIGGERS } from '../util/constants';
import redditClient from '../clients/reddit';
import reportError from '../util/errorReporting';
// import logger from '../util/logger';

const { WEEKLY_WOW_THREAD_TARGET_CHANNEL_ID } = process.env;

function get(req, res) {
    res.status(400).json('not implemented');
}

async function getWeeklyWow(req, res) {
    // TODO: 'validate' these requests - see app engine docs
    const { topic } = req.query;

    if (!topic) {
        return res.status(400).json('must specify topic parameter');
    }

    try {
        const result = await redditClient.getWeeklyWowThread(WEEKLY_WOW_THREAD_TRIGGERS[topic]);

        if (!result) {
            throw new Error(`no result returned for weekly wow thread topic: ${topic}`);
        }

        const { title, url } = result;

        const messageString = [
            'Check out today\'s weekly thread on /r/wow!:',
            '',
            `${title} ${url}`
        ].join('\n');

        await req.bots.soupBot.sendMessage({
            to: WEEKLY_WOW_THREAD_TARGET_CHANNEL_ID,
            message: messageString
        });

        return res.status(200).send();
    } catch (err) {
        reportError(err);
        console.error('[cronController - getWeeklyWow]', err);

        return res.status(500).json({
            message: err.message
        });
    }
}

export default {
    get,
    getWeeklyWow
};
