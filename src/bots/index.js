import DiscordBot from './discordBot';
import messagesSubscriber from './discordBot/messages';
import voiceStateUpdatesSubscriber from './discordBot/voiceStateUpdates';

const soupBot = new DiscordBot({
    name: 'originalSoupbot',
    token: process.env.DISCORD_TOKEN_SOUPBOT,
    messagesSubscriber,
    voiceStateUpdatesSubscriber
});
// TODO: set up catch/error Report on this
// TODO: actually make it so it can fail - right now it would just hang I think
soupBot.init();

export default {
    soupBot
};
