import { messageTrigger } from '../../../../util/operators';
import { getRandomElement } from '../../../../util/helpers';

import recipes from './recipes';

export default source => source
    .pipe(
        messageTrigger('soup')
    )
    .subscribe(({ message, bot }) => {
        const { title, image_url: imageUrl } = getRandomElement(recipes);
        const response = `Coming up, some delicious ${title} for the group \n ${imageUrl}`;

        bot.sendMessage(message.channel, response);
    });
