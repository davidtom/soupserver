import { from } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';

import { messageTrigger } from '../../../util/operators';
import redditClient from '../../../clients/reddit';

const HELP_TEXT =
    'Description: gets a random top post from the past day from the specified subreddit \n' +
    'Usage: reddit [subreddit] \n' +
    'Default subreddit: wow';

export default source => source
    .pipe(
        messageTrigger('reddit'),
        mergeMap(({ bot, message }) => {
            let subreddit = message.content.split(' ')[1];

            if (subreddit === 'help') {
                return from([{
                    response: HELP_TEXT,
                    bot,
                    message
                }]);
            }

            if (!subreddit) {
                subreddit = 'wow';
            }

            // NOTE: convert to a real promise via Promise.all()
            // see: https://github.com/not-an-aardvark/snoowrap/issues/37
            return from(Promise.all([
                redditClient.getRandomTop(subreddit, 'day'),
            ]))
                .pipe(
                    map(([ result ]) => {
                        const { title, url } = result;
                        const messageString = `${title} ${url}`;

                        return {
                            response: messageString,
                            bot,
                            message
                        };
                    })
                );
        })
    )
    .subscribe(({ message, response, bot }) => {
        bot.sendMessage(message.channel, response)
    });
