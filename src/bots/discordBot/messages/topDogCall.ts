import discordBot from '../index';
import { messageTrigger } from '../../../util/operators';

export default source => source
    .pipe(
        messageTrigger('topDogCall')
    )
    // TODO: make input type an interface and use here and in discordBot class
    .subscribe(({ bot }: { bot: discordBot, message: string}) => {
        // TODO: if this is called when not in a channel it breaks - fix/handle that
        // TODO: scope this so only topdogs can do it?
        const fileName = '/topDogCallV1.mp3';
        bot.playAudio(fileName);
    });
