import { WEEKLY_WOW_THREAD_TRIGGERS } from '../../../../../util/constants';

import weeklyThreadSubscriberFactory from './weeklyThreadSubscriberFactory';

// NOTE: exports the following array:
// [
//     weeklyThreadSubscriberFactory('pvp', 'flair:PVP+Sunday'),
//     weeklyThreadSubscriberFactory('newb', 'flair:Murloc+Monday'),
//     weeklyThreadSubscriberFactory('tank', 'flair:Tanking+Tuesday'),
//     weeklyThreadSubscriberFactory('heals', 'flair:Midweek+Mending'),
//     weeklyThreadSubscriberFactory('loots', 'flair:Loot+Thread'),
//     weeklyThreadSubscriberFactory('dps', 'flair:Firepower+Friday'),
// ]

export default Object.keys(WEEKLY_WOW_THREAD_TRIGGERS)
    .map(trigger => weeklyThreadSubscriberFactory(trigger, WEEKLY_WOW_THREAD_TRIGGERS[trigger]));
