import { from } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';

import { messageTrigger } from '../../../../../util/operators';
import redditClient from '../../../../../clients/reddit';
// import reportError from 'util/errorReporting';

// TODO: make this available on a route, so I can hit it with a chron

// TODO: error handle all observable chains
export default (trigger, topic) => source => source
    .pipe(
        messageTrigger(trigger),
        mergeMap(({ bot, message }) =>
            // convert to a real promise via Promise.all()
            // see: https://github.com/not-an-aardvark/snoowrap/issues/37
            from(Promise.all([
                redditClient.getWeeklyWowThread(topic || trigger)
            ]))
                .pipe(
                    map(([ topResult ]) => {
                        let title;
                        let url;
                        if (topResult) {
                            ({ title, url } = topResult);
                        } else {
                            const e = new Error(`No result found for trigger: ${trigger} / topic: ${topic}`);
                            // TODO: report error
                            console.error(e);
                            // reportError(e);

                            ({ title, url } = redditClient.getErrorResponse());
                        }

                        const response = `${title} ${url}`;

                        return {
                            response,
                            bot,
                            message
                        };
                    })
                ))
    )
    .subscribe(({ message, bot, response }) => {
        bot.sendMessage(message.channel, response)
    });
