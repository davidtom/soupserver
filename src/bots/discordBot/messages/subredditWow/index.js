import weeklyThreadSubscribers from './weeklyThreads';

export default [
    ...weeklyThreadSubscribers,
];
