import { messageTrigger } from '../../../../util/operators';

export default source => source
    .pipe(
        messageTrigger('!channelId')
    )
    .subscribe(({ message, bot }) => {
        bot.sendMessage(message.channel, message.channel.id);
    });
