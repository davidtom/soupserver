import describeChannelSubscriber from './getChannelId';
import describeUserSubscriber from './describeUser';

export default [
    describeChannelSubscriber,
    describeUserSubscriber
];
