import { messageTrigger } from '../../../../util/operators';

export default source => source
    .pipe(
        messageTrigger('!whoami')
    )
    .subscribe(({
        message,
        bot,
    }) => {
        const { author } = message;
        console.log(author)
        bot.sendMessage(message.channel, `name: ${author.username}\nid: ${author.id}`)
    });
