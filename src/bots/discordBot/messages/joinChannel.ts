import discordBot from '../index';
import { messageTrigger } from '../../../util/operators';

export default source => source
    .pipe(
        messageTrigger('joinChannel')
    )
    // TODO: make input type an interface and use here and in discordBot class
    .subscribe(({ message, bot }) => {
        if (!message.guild) {
            bot.sendMessage(message.channel, 'Unable to join: command must come from a message in the server where channel exists');
            return
        }
        // TODO: scope this so only topdogs can do it
        const { member } = message;
        const { voiceChannel } = member;

        if (!voiceChannel) {
            const response = `User ${member.user.username} is not currently in a voice channel`;
            bot.sendMessage(message.channel, response);
            return
        }

        voiceChannel.join()
            .then(() => {
                bot.setVoiceChannelId(voiceChannel.id);
                const response = `Joined voice channel ${voiceChannel.name}`
                bot.sendMessage(message.channel, response);
            }).
            catch(e => console.error('error joining voice channel', e))
    });
