import { messageTrigger } from '../../../util/operators';

export default source => source
    .pipe(
        messageTrigger('flipCoin')
    )
    .subscribe(({ bot, message }) => {
        const result = (Math.floor(Math.random() * 2) + 1) === 1 ? 'heads' : 'tails';
        bot.sendMessage(message.channel, result);
    });
