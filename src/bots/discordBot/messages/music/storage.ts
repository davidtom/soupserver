// TODO: this is just temporary - have Evan help me set up a storage client and a real DB
class Storage {
    private collection: string[];

    constructor() {
        this.collection = []
    }

    save(doc) {
        this.collection.push(doc)
    }

    list() {
        return this.collection.join('\n');
    }
};

const storage = new Storage();

export default storage;
