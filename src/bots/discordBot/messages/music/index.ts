import saveSubscriber from './save';
import listSubscriber from './list';

export default [
    saveSubscriber,
    listSubscriber,
];
