import { messageTrigger } from '../../../../util/operators';
import storage from './storage';

const RYTHM_REGEXP = /^Rythm/
const filter = response => RYTHM_REGEXP.test(response.author.username);

const parseSongFromMessage = (msg) => {
    const rawDescription = msg && msg.embeds && msg.embeds[0] && msg.embeds[0].description;
    // TODO: this returns undefined, but should probably throw an error? the consumer handles it?
    const description = rawDescription && rawDescription.split('\n')[0];
    return description;
}

export default source => source
    .pipe(
        messageTrigger('!np save')
    )
    .subscribe(({
        message,
    }) => {
        // Message Collector
        // https://discordjs.guide/popular-topics/collectors.html#basic-message-collector
        // TODO: handle errors and missed messages
        // TODO: handle "not connected" message
        message.channel.awaitMessages(filter, { max: 1, time: 10000, errors: ['time'] })
		.then(collected => {
            const data = parseSongFromMessage(collected.first());

            message.channel.send(`Saved song: ${data}`);

            storage.save(data);
			message.channel.send(`Currently saved songs:${storage.list()}`);
        })
        .catch(console.error)
    });
