import { messageTrigger } from '../../../../util/operators';
import storage from './storage';

export default source => source
    .pipe(
        messageTrigger('saved')
    )
    .subscribe(({
        message,
    }) => {
        message.channel.send(`Currently saved songs:${storage.list()}`);
    });
