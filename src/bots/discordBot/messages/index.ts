import generalMessageSubscribers from '../../messages';

import soupSubscriber from './soup';
import flipCoinSubscriber from './flipCoin';
import rollSubscriber from './roll';
import subredditTopSubscriber from './subredditTop';
import subredditWowSubscribers from './subredditWow';
import devUtilitySubscribers from './devUtility';
import joinChannelSubscribers from './joinChannel';
import topDogCallSubscribers from './topDogCall';
import musicSubscribers from './music';

// TODO: make this dynamic so I don't have to manually add stuff here
const messageSubscribers = [
    ...generalMessageSubscribers,
    soupSubscriber,
    flipCoinSubscriber,
    rollSubscriber,
    subredditTopSubscriber,
    ...subredditWowSubscribers,
    ...devUtilitySubscribers,
    joinChannelSubscribers,
    topDogCallSubscribers,
    ...musicSubscribers,
];

// returns an array of subscription objects
const messagesSubscriber = source => messageSubscribers
    // TODO: dont do this
    // @ts-ignore
    .map(subscriber => subscriber(source));

export default messagesSubscriber;
