import { map } from 'rxjs/operators';

import { messageTrigger } from '../../../util/operators';

const DEFAULT_CEILING = 100;

const getMessage = (result, denominator) => {
    if (denominator !== DEFAULT_CEILING) {
        return `${result} / ${denominator}`;
    }

    return result;
};

export default source => source
    .pipe(
        messageTrigger('roll'),
        map(({ bot, message }) => {
            let ceiling = DEFAULT_CEILING;

            const secondArg = message.content.split(' ')[1];
            const parsedSecondArg = parseInt(secondArg, 10);

            if (!Number.isNaN(parsedSecondArg) && parsedSecondArg > 0) {
                ceiling = parsedSecondArg;
            }

            return {
                bot,
                message,
                ceiling
            };
        })
    )
    .subscribe(({ bot, message, ceiling }) => {
        const result = (Math.floor(Math.random() * ceiling) + 1);
        bot.sendMessage(message.channel, getMessage(result, ceiling))
    });
