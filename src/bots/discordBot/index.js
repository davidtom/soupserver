import * as path from 'path'
import { Observable, Subscription, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import * as Discord from 'discord.js';

import Bot from '../Bot';

// TODO: set up debugger so I can explore bot properties easier

// TODO: Figure out the path to the file, then make a config
// TODO: should these be class properties?
const AUDIO_DIR = path.join(process.cwd(), path.join('/assets/audio'))
const SERVER_ID = process.env.SERVER_ID;

export default class DiscordBot extends Bot {
    constructor({
        name,
        token,
        messagesSubscriber,
        voiceStateUpdatesSubscriber
    } = {}) {
        if (!token) {
            throw new Error('invalid bot creation: a token must be supplied');
        }

        super(name);

        // TODO: rename to client
        this.bot = new Discord.Client();
        this.token = token;
        this.subscription = new Subscription();
        this.messagesSubscriber = messagesSubscriber;
        this.voiceStateUpdatesSubscriber = voiceStateUpdatesSubscriber;

        this.message$ = this._createMessageObservable();
        this.voiceStateUpdate$ = this._createVoiceStateUpdateObservable();
        this.disconnect$ = this._createDisconnectObservable();

        // Set up listener for whenever the bot is ready;
        // ready means: connected, sorted data, and is ready to be interacted with
        this.bot.on('ready', () => {
            this.UPTIME_CONNECTED_AT = Date.now();

            // wait to assign serverId until client is connected;
            this._serverId = SERVER_ID;
            const { username, id } = this.bot.user;

            console.log(`Logged in as ${username} - ${id}`);

            this.initResolve();
        });

        // TODO: setup ready checks on the bot (bot is now accessible via req.bots?)
        // TODO: add route logger
        // TODO: create audio commands (see discordio.io/examples/audio)
        // TODO: create database to save responses (and delete them)
        // TODO: simple/basic, but functioning, unit tests/integ tests (probably just to test connection/message listening work)
        // TODO: set up error reporting source maps
        // TODO: go back and catch/reportErrors
        // TODO: findOrCreate users from a document store to 'validate' them
        // TODO: dockerize dev environment
        // TODO: add test coverage and pipeline status to readme/project
        // TODO: solve 'disconnected Already Authenticated 4005' error
        // TODO: use node-opus instead of opusscript (need to getting working on alpine though...)
    }

    init() {
        // initialize message handlers
        const messageSubscriptions = this.messagesSubscriber(this.message$);
        messageSubscriptions.forEach(subscription => this.subscription.add(subscription));


        // initialize voiceStateUpdate handler
        const voiceStateUpdateSubscriptions = this.voiceStateUpdatesSubscriber(this.voiceStateUpdate$);
        voiceStateUpdateSubscriptions.forEach(subscription => this.subscription.add(subscription));

        // log on disconnects
        const disconnectSubscription = this.disconnect$
            .subscribe(({ wasClean, code, reason }) => {
                const cleanStatus = wasClean ? 'cleanly' : 'uncleanly';
                console.log(`${this.name} disconnected ${cleanStatus} with code ${code} for reason ${reason}`);
            });

        this.subscription.add(disconnectSubscription);

        // begin connection process
        this.connect();

        return this.initPromise;
    }

    _createMessageObservable() {
        const messageSubject = new Subject();
        Observable.create((observer) => {
            this.bot.on('message', (message) => {
                const bot = this;
                try {
                    observer.next({ message, bot });
                } catch (err) {
                    console.log("ERROR", err)
                    observer.error(err);
                }
            });
        }).subscribe(messageSubject);
        return messageSubject;
    }

    // TODO: migrate to discord.js? seems better supported and has better docs - might even be typed!
    // https://www.reddit.com/r/discordapp/comments/6p85uf/discordjs_any_way_to_detect_when_someone_enter_a/
    _createVoiceStateUpdateObservable() {
        const voiceStateUpdateSubject = new Subject();
        Observable.create((observer) => {
            this.bot.on('voiceStateUpdate', (oldMember, newMember) => {
                const bot = this;
                try {
                    observer.next({
                        oldMember,
                        newMember,
                        bot
                    })
                } catch (e) {
                    observer.error(e);
                }
            });
        }).subscribe(voiceStateUpdateSubject);
        return voiceStateUpdateSubject;
    }

    _createDisconnectObservable() {
        return Observable.create((observer) => {
            this.bot.on('disconnect', (closeEvent) => {
                const { type, wasClean, reason, code } = closeEvent;
                const bot = this;
                try {
                    observer.next({ type, wasClean, reason, code, bot });
                } catch (e) {
                    observer.error(e);
                }
            });
        });
    }

    // TODO: jsdoc for input params and return value/type
    sendMessage(channel, message) {
        // TODO: error handle
        return channel.send(message);
    }

    connect() {
        console.log(`${this.name} attempting to connect`);
        this.bot.login(this.token);
    }

    disconnect() {
        this._serverId = undefined;
        return this.bot.destroy();
    }

    close() {
        if (!this.initialized) {
            return Promise.resolve();
        }

        const disconnectPromise = this.disconnect$.pipe(
            take(1)
        ).toPromise().then(() => {
            this.subscription.unsubscribe();
            this.initialized = false;
            console.log(`${this.name} has successfully closed`);
        });

        this.disconnect();

        return disconnectPromise;
    }

    // TODO: this should get set if/when the bot is moved to a new channel by a user (not by the joinChannel command)
    setVoiceChannelId(voiceChannelId) {
        this._voiceChannelId = voiceChannelId;
    }

    get voiceChannelId() {
        return this._voiceChannelId;
    }

    get serverId () {
        return this._serverId;
    }

    playAudio(filename) {
        const audioPath = path.join(AUDIO_DIR, filename);
        // get the voice connection for the server
        const connection = this.bot.voice.connections.get(SERVER_ID);

        console.log(`Playing ${filename}`);
        this.dispatcher = connection.playFile(audioPath);

        this.dispatcher.on('end', () => {
            console.log(`Completed playing ${filename}`);
        });
        this.dispatcher.on('error', e => {
            // Catch any errors that may arise
            console.error(`Error playing ${filename}`, e);
        });
    }
}
