import topDogJoinedSubscriber from './topDogJoined';

const voiceStateUpdateSubscribers = [
    topDogJoinedSubscriber
];

// returns an array of subscription objects
const voiceStateUpdatesSubscriber = source => voiceStateUpdateSubscribers
    .map(subscriber => subscriber(source));

export default voiceStateUpdatesSubscriber;
