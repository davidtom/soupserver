import { filter } from 'rxjs/operators';

export default source => source
    .pipe(
        // TODO: must be in voice channel - make custom operator
        filter(({ bot }) => bot.voiceChannelId),
        // TODO: user joins channel - make a custom operator
        filter(({ oldMember, newMember, bot }) => {
            const { voiceChannel: oldChannel } = oldMember;
            const { voiceChannel } = newMember;

            // member leaving - dont proceed
            if (!voiceChannel) {
                return false;
            }

            // member doing action in channel - don't proceed
            if (oldChannel && oldChannel.id === voiceChannel.id) {
                return false;
            }

            return voiceChannel.id === bot.voiceChannelId;
        }),
        // TODO: must be a top dog - make custom operator
        filter(({ newMember, bot }) => {
            const topDogRole = bot.bot.guilds.get(bot.serverId).roles.find(role => role.name === 'Top Dog');
            const memberRoles = newMember.guild.roles.keyArray();
            return memberRoles.includes(topDogRole.id);
        })
    )
    .subscribe(({ user, bot}) => {
        const fileName = '/topDogCallV2.mp3';
        // TODO: for current error I see, I think I need to wait until bot's connection to channel initialized
        // also seems like something I'm doing in joinChannel - maybe the ordering of stuff - is causing the channelId error
        bot.playAudio(fileName);
    });
