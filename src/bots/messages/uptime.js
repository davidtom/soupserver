import { messageTrigger } from '../../util/operators';

export default source => source
    .pipe(
        messageTrigger('uptime')
    )
    .subscribe(({ message, bot }) => {
        bot.sendMessage(message.channel, bot.getUptimeStatsMessage())
    });
