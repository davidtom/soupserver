import { msTimeConversion } from '../util/helpers';

export default class Bot {
    constructor(name) {
        this.name = name;
        this.initialized = false;

        // uptime stats
        this.UPTIME_BOT_CREATED_AT = Date.now();
        this.UPTIME_CONNECTED_AT = undefined;
        this.messageCounters = {};

        const that = this;
        this.initPromise = new Promise((resolve, reject) => {
            that.initResolve = resolve;
            that.initReject = reject;
        }).then(() => { that.initialized = true; });
    }

    getUptimeStatsMessage() {
        const processUptimeMs = process.uptime() * 1000;
        const botUptimeMs = Date.now() - this.UPTIME_BOT_CREATED_AT;
        const connectedTimeMs = Date.now() - this.UPTIME_CONNECTED_AT;
        const messageStats = Object.keys(this.messageCounters)
            .map(messageName => `\t${messageName}: ${this.messageCounters[messageName]}`)
            .join('\n');

        // NOTE: join array of strings since multiline template literal get indented strangely
        return [
            `My main process has been online for ${msTimeConversion(processUptimeMs)}.`,
            `I have been alive for ${msTimeConversion(botUptimeMs)}.`,
            `My current connection has been active for ${msTimeConversion(connectedTimeMs)}.`,
            `I have responded to the following commands:\n${messageStats}`
        ].join('\n');
    }

    incrementMessageCounter(name) {
        if (!this.messageCounters[name]) {
            this.messageCounters[name] = 0;
        }

        this.messageCounters[name] += 1;
    }
}
