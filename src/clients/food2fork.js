import * as request from 'superagent';

import config from '../util/config';

export default class Food2Fork {
    constructor() {
        const { FOOD_URL, FOOD_2_FORK_API_KEY } = config();

        this.url = FOOD_URL;
        this.key = FOOD_2_FORK_API_KEY;
    }

    get(query) {
        return request
            .get(this.url)
            .query({
                q: query,
                key: this.key
            })
            .then((resp) => {
                const { text } = resp;
                if (!text) {
                    throw new Error('Unexpected response format');
                }

                const data = JSON.parse(text);

                return data.recipes || [];
            })
            .catch((err) => {
                // setup debug (and a logger)
                console.error('[food2fork]', err);
            });
    }
}
