import * as Snoowrap from 'snoowrap';

import { getRandomElement } from '../util/helpers';

const ERROR_RESPONSES = [
    {
        title: 'Something went wrong!',
        url: 'https://media.giphy.com/media/aJnQ9P7JVR25W/giphy.gif'
    },
    {
        title: 'You broke it, way to go!',
        url: 'https://media.giphy.com/media/3o7btQwpidxMWWZ4oE/giphy.gif'
    },
    {
        title: 'You broke the bot!',
        url: 'https://media.giphy.com/media/3o85g2ttYzgw6o661q/giphy.gif'
    }
];

export class RedditClient {
    constructor() {
        this.r = new Snoowrap({
            userAgent: 'discord:discordBot:v1 (by /u/Shabla_goo32)',
            // TODO: use config?
            clientId: process.env.REDDIT_CLIENT_ID,
            clientSecret: process.env.REDDIT_CLIENT_SECRET,
            username: process.env.REDDIT_USERNAME,
            password: process.env.REDDIT_PASSWORD
        });
    }

    getErrorResponse() {
        return getRandomElement(ERROR_RESPONSES);
    }

    getRandomTop(subreddit, time) {
        return this.r
            .getSubreddit(subreddit)
            .getTop({
                time
            })
            .then(results => getRandomElement(results))
            .then((result) => {
                const { title, permalink, url } = result;
                const redditUrl = `https://www.reddit.com${permalink}`;

                return {
                    title,
                    url,
                    redditUrl
                };
            })
            .catch((e) => {
                // TODO: this error is huge, pick out what I need from it
                // TODO: report error
                console.error('[reddit client: getRandomTop]', e);

                return this.getErrorResponse();
            });
    }

    searchSubreddit({
        subreddit,
        query,
        time,
        sort
    } = {}) {
        return this.r
            .getSubreddit(subreddit)
            .search({
                query,
                time,
                sort,
            })
            .catch((e) => {
                // TOOD: this error is huge, pick out what I need from it
                console.error('[reddit client: getRandomTop]', e);

                return this.getErrorResponse();
            });
    }

    getWeeklyWowThread(query) {
        return this.searchSubreddit({
            subreddit: 'wow',
            query,
            time: 'month',
            sort: 'new',
        })
            .then(results => results[0]);
    }
}

export default new RedditClient();
