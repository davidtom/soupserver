FROM node:10.10-alpine

RUN apk update && \
    apk add ffmpeg

WORKDIR /app

COPY . .

RUN yarn install --pure-lockfile
RUN npm run build

CMD [ "node", "dist/index.js" ]

# TODO: add a second stage that doesn't have dev dependencies
